"use strict";

(function(exports){

  function person(options){
    if ( typeof person.counter == 'undefined' ) {
      person.counter = 0;
    }

    person.counter += 1;
    this.sn = '';
    this.givenName = '';
    this.telephoneNumber = '';
    this.mail = '';
    this.jpegPhoto = '';
    this.equipe = '';
    this.id = person.counter;

    // get options
    for(var ii in options){
      this[ii]=options[ii];}
  };


  var selected_person = undefined;
  let selected_person_null = function(){
    console.log(selected_person)
    $('#info_'+selected_person).hide() ;

    //var selected_person = undefined;
  };



  function createTrombinoscope(persons){

    var where = d3.select('#trombinoscope');
    var nodes = where
	  .append('div')
	  .attr('id','allperson')
	  .selectAll('.person')
	  .data(persons)
	  .enter()
  	  .append('div')
  	  .attr('class','image-cropper')
	  .on('click',function(e){
	    selected_person = e.id;
	    d3.event.stopPropagation();
	    d3.select('#info_'+e.id)
	      .style('display','block');})
    ;

    let nomediv = nodes.append('div')
	  .attr('class','nomediv');


    nomediv.append('div')
	  .text(function(e){
	    return e.givenName ;
	  });
    nomediv.append('div')
      .text(function(e){
	return e.sn ;
      });


    var photo = nodes
	  .append('img')
	  .attr('class','person')
	  .attr('src',function(e){return e.jpegPhoto;})
	  .attr('alt',"Avatar")
	  .on("error",function(){this.style.display='none';})
    ;

    var infos = nodes
	  .append('div')
	  .attr('class','modal')
	  .attr('id',function(e){return 'info_'+e.id;})
	  .on('click',function(e){
	    if(d3.event.target==this){
	      d3.event.stopPropagation();
	      d3.select('#info_'+e.id)
		.style('display','none');
	    }
	  })
	  .append("div")
	  .attr('class','modal-content')
    ;

    infos.append("span")
      .attr('class','closeBox')
      .on('click',function(e){
	d3.event.stopPropagation();
	d3.select('#info_'+e.id)
	  .style('display',"none");
	selected_person = undefined;
      })
    ;

    infos.append('div')
      .attr("class",'carte')
      .html(function(e){
	let art = ''+
	  ' <div>'+
	  '  <p>'+e.displayName +'</p>'+
	  '  <a email href="mailto:'+e.mail+'">'+e.mail+'</a>' +
	  '  <p> equipe : '+e.equipe+'</p>'+
	  '  <p> tel : '+e.telephoneNumber+'</p>'+
          ' </div>'+
	  ' <div>'+
	  '  <img onerror="this.style.display=\'none\'" src="'+e.jpegPhoto+'">'+
	  '  </img>'+
	  ' </div>'
	;

	return art;})
    ;


    window.onclick = function(event) {
      selected_person_null();
    };

    document.onkeydown = function(evt) {
      evt = evt || window.event;
      if (evt.keyCode == 27) {
	console.log(selected_person)
	selected_person_null();
      }
    };
  }


  jQuery.getJSON("luthMembers.json",function(data){


    let persons = [];
    for(let ii=0;ii<data.length;ii++){
      let loc_person = data[ii];
      if( loc_person.hasOwnProperty('jpegPhoto')){
	loc_person.jpegPhoto = 'data:image/png;base64,' + loc_person.jpegPhoto;
      }
      // console.log(data[ii])
      persons.push(new person(data[ii]));


    }


    createTrombinoscope(persons);

  });




})(this.Trombi = {});
