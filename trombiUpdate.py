#!/usr/bin/python
# -*- coding: utf-8 -*-

import ldap, sys, time
import os
import json

# Define variables
basedn = "ou=luth,dc=obspm,dc=fr"

def main():
    # Init de la connection anonyme au ldap
    try:
        ld = ldap.initialize('ldap://ldap.obspm.fr:389', )
    except ldap.LDAPError as e:
        print(e)

    # liasion
    ld.simple_bind_s()

    # Requete
    results = ld.search_s(basedn,ldap.SCOPE_SUBTREE, 'sn=*')
    #results = ld.search_s(basedn,ldap.SCOPE_SUBTREE, 'uid=mottez')


    # Output ldif format if needed
    return results

if __name__ == '__main__':

    attributes = ('sn','givenName','mail','displayName','description','l','departmentNumber','employeeType','jpegPhoto','roomNumber','telephoneNumber')
    results = main()

    good_results = []
    for ii in range(0,len(results)) :
        locdict = {}
        for attr in attributes :
            value = results[ii][1].get(attr,'')
            if isinstance(value,list) : value = value[0]

            if attr == 'jpegPhoto' :
                value = value.encode('base64')
            elif attr == 'telephoneNumber' :
                print value
                value = value.replace(' ','')
                if len(value) == 4:
                    value = '14507' + value
                elif len(value) == 10 and value[0] == '0':
                    value = value[1:]
                print value
                if len(value) > 0:
                    svalue = iter(value[1:])
                    value = '+33 (0)'+value[1]+' '+  ' '.join(a+b for a,b in zip(svalue, svalue))
                print value
            else :
                pass
            locdict[attr] = value
        locdict['equipe'] = locdict['description']
        good_results.append(locdict)

    with open('luthMembers.json','w') as outfile :
        json.dump(good_results,outfile)
